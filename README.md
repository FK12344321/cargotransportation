<h1>Cargo Transportation</h1>
<p>By Krasilnikov Fedor v2.0.0<br>
<a href="https://gitlab.com/FK12344321/cargotransportation/-/tree/cont">Project gitlab page</a></p>
<h3>What it does</h3>
<p>The project is dedicated to model behaviour of transportation system containing two trucks and one ship.</p>
<img src="map.jpg" alt="Image of the map" height="100">
<p>User can interact with this system by sending json objects to it. As a respond to these objects system sends reports about the state of itself and stores them in a special object.</p>
<h3>Terms of use</h3>
<p>You are free to copy, modify, and distribute TimeCalculator.Cargo Transportation with attribution under the terms of the MIT license. See the <a href="license.txt">License</a> file for details.</p>
<h3>How to use</h3>
<p>To use the project you better should have:</p>
<ul>
<li>openjdk 11 or higher</li>
<li>Git</li>
</ul>
<p>To start using the program you need to launch the main function and write json objects to the command line. <br>
You can either supply a cargo object to some TimeCalculator.Point on the map and set a destination point for this cargo: <br>
> {"command": "cargo.supply", "route": {"source": "&lt;SOURCE_POINT>", "destination": "&lt;DESTINATION_POINT"}}<br><br>
or skip time to the nearest event: <br> > {"command": "time.advance"}</p>
<p>In this case SOURCE_POINT is an initial point of a cargo route (i.e. the point where you want to place a cargo). DESTINATION_POINT is a point 
where you want the cargo to be delivered. You have the following options: A, B, P, N.</p>
<p>You will recieve all the logs about transport arrival, departure, cargo supply and delivery via the console.</p>
<p>PS. If you want to use the system in some other way, you can import .jar file from the target directory (result of the project compilation) to your project and use the required api of the TimeCalculator.TimeCalculator class. Descriptions of the api methods can be found in the corresponding .java file. Examples of the using can be found in test/java/Tests.java file.
Check <a href="design.md">design file</a> to know how</p>
<h3>Other documentation</h3>
<a href="action_uml.png">Action diagram</a><br>
<a href="arc_uml.png">Class diagram</a>
