import java.util.*;

import map_model.TimeCalculator;
import map_model.Repository;
import org.json.*;

public class Main {
    private static void processRequest(TimeCalculator timeCalculator, JSONObject json) {
        String command = json.get("command").toString();
        if (command.equals("cargo.supply")) {
            JSONObject report = timeCalculator.supplyCargo(json);
            System.out.println(report);
        }
        else if (command.equals("time.advance")) {
            List<JSONObject> reports = timeCalculator.timeAdvance();
            for (JSONObject report : reports) {
                System.out.println(report);
            }
        }
    }

    public static void main(String[] args) {
        TimeCalculator timeCalculator = new TimeCalculator(new Repository());
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            JSONObject json = new JSONObject(scanner.nextLine());
            processRequest(timeCalculator, json);
        }
    }
}
