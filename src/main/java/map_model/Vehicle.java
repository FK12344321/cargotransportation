package map_model;

import map_model_interfaces.Place;
import map_model_interfaces.Transport;
import map_model_interfaces.Transportable;

import java.util.List;

public class Vehicle implements Transport {
    private Place dest;
    private int timeToReturn;
    private int timeToDeliver;
    private final List<Place> possibleDest;
    private Place initial;
    private boolean delivered;
    private final int id;
    private final String type;
    private Transportable currentCargo = null;

    public Vehicle(Place initial, List<Place> possibleDest, String type, int id) {
        this.dest = null;
        this.timeToReturn = 0;
        this.timeToDeliver = 0;
        this.possibleDest = possibleDest;
        this.initial = initial;
        this.id = id;
        this.type = type;
    }

    public Place getDest() {
        return dest;
    }

    public void setDest(Place destination) {
        dest = destination;
    }

    public int getTimeToReturn() {
        return timeToReturn;
    }

    public void setCurrentCargo(Transportable cargo) {
        currentCargo = cargo;
    }

    public void setTimeToReturn(int time) {
        timeToReturn = time;
    }

    public int getTimeToDeliver() {
        return timeToDeliver;
    }

    public void setTimeToDeliver(int time) {
        timeToDeliver = time;
    }

    public boolean canGoTo(Place point) {
        return possibleDest.contains(point);
    }

    public int getID() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Place getInitial() {
        return initial;
    }

    public void setInitial(Place point) {
        initial = point;
    }

    public Transportable getCurrentCargo() {
        return currentCargo;
    }

    public void decreaseTime(int time) {
        timeToDeliver -= time;
        timeToReturn -= time;
        if (timeToDeliver <= 0 && !delivered) {
            delivered = true;
        }
    }
}
