package map_model;

import map_model_interfaces.Place;
import map_model_interfaces.Transportable;

import java.util.ArrayList;

public class Point implements Place {
    private final ArrayList<Transportable> cargoes = new ArrayList<>();
    private final String name;

    public Point(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addCargo(Transportable cargo) {
        cargoes.add(cargo);
    }

    public Transportable popCargo() {
        return cargoes.remove(cargoes.size() - 1);
    }

    public boolean isEmpty() {
        return cargoes.isEmpty();
    }
}