package map_model;

import java.util.*;

import map_model_interfaces.Place;
import map_model_interfaces.Transport;
import map_model_interfaces.Transportable;
import org.json.*;

public class TimeCalculator {
    private final HashMap<Place, HashMap<Place, Integer>> map =
            new HashMap<>();
    private final HashMap<String, Place> cargoesToPoints =
            new HashMap<>();
    private final Place pointS;
    private final Place pointP;
    private final Place pointA;
    private final Place pointB;
    private int nextCargoID = 0;
    private final ArrayList<Transport> vehiclesOnMap = new ArrayList<>();
    private ArrayList<JSONObject> currentReports = new ArrayList<>();
    private int currentTime = -1;
    private final Repository repo;

    public TimeCalculator(Repository repo) {
        this.pointS = new Point("S");
        this.pointP = new Point("P");
        this.pointA = new Point("A");
        this.pointB = new Point("B");
        this.repo = repo;
        createMap();
        createVehicles();
        createCargoesToPoints();
    }

    private void addReport(JSONObject json) {
        currentReports.add(json);
        repo.addReport(json);
    }

    private void createCargoesToPoints() {
        cargoesToPoints.put("S", pointS);
        cargoesToPoints.put("P", pointP);
        cargoesToPoints.put("A", pointA);
        cargoesToPoints.put("B", pointB);
    }

    private HashMap<Place, Integer> createRow(Integer sValue, Integer pValue, Integer aValue, Integer bValue) {
        HashMap<Place, Integer> row = new HashMap<>();
        row.put(pointS, sValue);
        row.put(pointP, pValue);
        row.put(pointA, aValue);
        row.put(pointB, bValue);
        return row;
    }

    private void createMap() {
        HashMap<Place, Integer> sRow = createRow(null, 1, 5, null);
        map.put(this.pointS, sRow);
        HashMap<Place, Integer> pRow = createRow(1, null, null, 4);
        map.put(this.pointP, pRow);
        HashMap<Place, Integer> aRow = createRow(5, null, null, null);
        map.put(this.pointA, aRow);
        HashMap<Place, Integer> bRow = createRow(null, 4, null, null);
        map.put(this.pointB, bRow);
    }

    private Vehicle createCar(int id) {
        ArrayList<Place> carDestinations = new ArrayList<>();
        carDestinations.add(this.pointA);
        carDestinations.add(this.pointP);
        return new Vehicle(this.pointS, carDestinations, "truck", id);
    }

    private Vehicle createShip(int id) {
        ArrayList<Place> shipDestinations = new ArrayList<>();
        shipDestinations.add(this.pointB);
        return new Vehicle(this.pointP, shipDestinations, "ship", id);
    }

    private void createVehicles() {
        vehiclesOnMap.add(createShip(0));
        vehiclesOnMap.add(createCar(1));
        vehiclesOnMap.add(1, createCar(2));
    }

    private int findTimeAdvance() {
        if (currentTime == -1) {
            currentTime = 0;
            return 0;
        }
        int min = 100;
        for (Transport vehicle : vehiclesOnMap) {
            if (min > vehicle.getTimeToDeliver() && vehicle.getTimeToDeliver() > 0) min = vehicle.getTimeToDeliver();
            if (min > vehicle.getTimeToReturn() && vehicle.getTimeToReturn() > 0) min = vehicle.getTimeToReturn();
        }
        return min;
    }

    void resetVehicle(Transport vehicle, Place destination, Transportable cargo) {
        vehicle.setTimeToDeliver(map.get(vehicle.getInitial()).get(destination));
        vehicle.setTimeToReturn(vehicle.getTimeToDeliver() * 2);
        vehicle.setDest(destination);
        vehicle.setCurrentCargo(cargo);
    }

    private void sendCargo(Transport vehicle) {
        Transportable cargo = vehicle.getInitial().popCargo();
        Place destination =
                vehicle.canGoTo(cargoesToPoints.get(cargo.getDest())) ?
                        cargoesToPoints.get(cargo.getDest()) : pointP;
        resetVehicle(vehicle, destination, cargo);
        JSONObject departed = JSONHandler.createDepartedJSON(currentTime, vehicle);

        addReport(departed);
    }

    private void returnToInitial(Transport vehicle) {
        Place temp = vehicle.getDest();
        vehicle.setDest(vehicle.getInitial());
        vehicle.setInitial(temp);
        JSONObject departed = JSONHandler.createDepartedJSON(currentTime, vehicle);
        addReport(departed);
    }

    private void redirectVehicles() {
        for (Transport vehicle : vehiclesOnMap) {
            if (vehicle.getTimeToReturn() == 0 && !vehicle.getInitial().isEmpty()) {
                sendCargo(vehicle);
            }
            else if (vehicle.getTimeToDeliver() == 0 && vehicle.getTimeToReturn() != 0) {
                returnToInitial(vehicle);
            }
        }
    }

    private void transportDeliver(Transport vehicle) {
        JSONObject arrived = JSONHandler.createArrivedJSON(currentTime, vehicle);
        addReport(arrived);
        if (vehicle.getDest() == pointP) vehicle.getDest().addCargo(vehicle.getCurrentCargo());
        else {
            JSONObject delivered = JSONHandler.createDeliveredJSON(currentTime, vehicle);
            addReport(delivered);
        }
        vehicle.setCurrentCargo(null);
    }

    private void transportReturn(Transport vehicle) {
        JSONObject arrived = JSONHandler.createArrivedJSON(currentTime, vehicle);
        addReport(arrived);
        vehicle.setDest(null);
    }

    private void passTime(int minTime) {
        for (Transport vehicle : vehiclesOnMap) {
            if (vehicle.getDest() == null) continue;
            if (vehicle.getTimeToDeliver() - minTime == 0) {
                transportDeliver(vehicle);
            }
            if (vehicle.getTimeToReturn() - minTime == 0) {
                transportReturn(vehicle);
            }
            vehicle.decreaseTime(minTime);
        }
    }

    /**
     * the method is used to supply an object to some point
     * for this you need to pass json in the following format:
     * {"command": "cargo.supply", "route": {"source": "&lt; SOURCE_POINT&gt;",
     * "destination": "&lt;DESTINATION_POINT&gt;"}}
     * @param cargoJSON json description of the cargo
     * @return json description of the last supplied object
     */
    public JSONObject supplyCargo(JSONObject cargoJSON) {
        JSONObject route = cargoJSON.getJSONObject("route");
        String destination = route.get("destination").toString();
        String source = route.get("source").toString();
        Cargo cargo = new Cargo(source, destination, nextCargoID);
        nextCargoID++;
        cargoesToPoints.get(source).addCargo(cargo);
        return JSONHandler.createSuppliedJSON(currentTime, cargo);
    }

    /**
     * the method is used to find and skip the time to
     * the closest event
     * @return array of reports generated within the last iteration
     */
    public List<JSONObject> timeAdvance() {
        int time = findTimeAdvance();
        currentReports = new ArrayList<>();
        currentTime += time;
        passTime(time);
        redirectVehicles();
        return currentReports;
    }
}

