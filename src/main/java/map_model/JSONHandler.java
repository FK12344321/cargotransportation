package map_model;

import map_model_interfaces.Transport;
import map_model_interfaces.Transportable;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONHandler {
    public static JSONObject createVehicleJSON(Transport vehicle) {
        JSONObject json = new JSONObject();
        json.accumulate("id", vehicle.getID());
        json.accumulate("type", vehicle.getType());
        return json;
    }

    public static JSONObject createMovementJSON(Transport vehicle) {
        JSONObject json = new JSONObject();
        json.accumulate("from", vehicle.getInitial().getName());
        json.accumulate("to", vehicle.getDest().getName());
        return json;
    }

    public static JSONArray createCargoesJSONArray(Transport vehicle) {
        JSONArray arr = new JSONArray();
        if (vehicle.getCurrentCargo() != null)
            arr.put(JSONHandler.createCargoJSON(vehicle.getCurrentCargo()));
        return arr;
    }

    public static JSONObject createDepartedJSON(int currentTime, Transport transport) {
        JSONObject json = new JSONObject();
        json.accumulate("event", "transport.departed");
        json.accumulate("time", currentTime);
        json.accumulate("transport", JSONHandler.createVehicleJSON(transport));
        json.accumulate("movement", JSONHandler.createMovementJSON(transport));
        json.put("cargoes", JSONHandler.createCargoesJSONArray(transport));
        return json;
    }

    public static JSONObject createDeliveredJSON(int currentTime, Transport transport) {
        JSONObject json = new JSONObject();
        json.accumulate("event", "cargo.delivered");
        json.accumulate("time", currentTime);
        json.accumulate("cargo", JSONHandler.createCargoJSON(transport.getCurrentCargo()));
        return json;
    }

    public static JSONObject createArrivedJSON(int currentTime, Transport transport) {
        JSONObject json = new JSONObject();
        json.accumulate("event", "transport.arrived");
        json.accumulate("time", currentTime);
        json.accumulate("transport", JSONHandler.createVehicleJSON(transport));
        json.accumulate("location", transport.getDest().getName());
        json.put("cargoes", JSONHandler.createCargoesJSONArray(transport));
        return json;
    }

    public static JSONObject createSuppliedJSON(int currentTime, Transportable cargo) {
        JSONObject json = new JSONObject();
        json.accumulate("event", "cargo.supplied");
        int time = Math.max(currentTime, 0);
        json.accumulate("time", time);
        json.accumulate("cargo", JSONHandler.createCargoJSON(cargo));
        return json;
    }

    public static JSONObject createCargoJSON(Transportable cargo) {
        JSONObject json = new JSONObject();
        JSONObject route = new JSONObject();
        json.accumulate("id", cargo.getID());
        route.accumulate("source", cargo.getInit());
        route.accumulate("destination", cargo.getDest());
        json.accumulate("route", route);
        return json;
    }
}
