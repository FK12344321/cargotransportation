package map_model;

import map_model_interfaces.Transportable;

public class Cargo implements Transportable {
    private final int id;
    private final String dest;
    private final String init;

    public Cargo(String init, String dest, int id) {
        this.id = id;
        this.init = init;
        this.dest = dest;
    }

    public int getID() {
        return id;
    }

    public String getDest() {
        return dest;
    }

    public String getInit() {
        return init;
    }
}