package map_model_interfaces;

public interface Transport {
    Place getDest();

    void setDest(Place destination);

    int getTimeToReturn();

    void setCurrentCargo(Transportable cargo);

    void setTimeToReturn(int time);

    int getTimeToDeliver();

    void setTimeToDeliver(int time);

    boolean canGoTo(Place point);

    int getID();

    String getType();

    Place getInitial();

    void setInitial(Place point);

    Transportable getCurrentCargo();

    void decreaseTime(int time);
}
