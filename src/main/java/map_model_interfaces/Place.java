package map_model_interfaces;

import map_model_interfaces.Transportable;

public interface Place {
    String getName();

    void addCargo(Transportable cargo);

    Transportable popCargo();

    boolean isEmpty();
}
