package map_model_interfaces;

public interface Transportable {
    int getID();

    String getDest();

    String getInit();
}
