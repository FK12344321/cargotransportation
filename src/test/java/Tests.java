import map_model.TimeCalculator;
import map_model.Repository;
import org.junit.jupiter.api.*;
import org.json.*;
import java.util.List;

class Tests {
    @Test
    void test1() {
        JSONObject supply1 = createSupplyJSON(createRoute("S", "B"));
        JSONObject report = timeCalculator.supplyCargo(supply1);
        JSONObject expectedResult = new JSONObject("{\"event\": \"cargo.supplied\", \"time\": 0, \"cargo\": {\"id\": 0, \"route\": {\"source\": \"S\", \"destination\": \"B\"}}}");
        Assertions.assertTrue(expectedResult.similar(report));
    }

    @Test
    void test2() {
        test1();
        JSONObject supply2 = createSupplyJSON(createRoute("S", "A"));
        JSONObject report = timeCalculator.supplyCargo(supply2);
        JSONObject expectedResult = new JSONObject("{\"event\": \"cargo.supplied\", \"time\": 0, \"cargo\": {\"id\": 1, \"route\": {\"source\": \"S\", \"destination\": \"A\"}}}");
        Assertions.assertTrue(expectedResult.similar(report));
    }

    @Test
    void test3() {
        test2();
        List<JSONObject> reports = timeCalculator.timeAdvance();
        JSONObject expectedReport1 = new JSONObject("{\"event\": \"transport.departed\", \"time\": 0, \"transport\": {\"id\": 1, \"type\": \"truck\"}, \"movement\": {\"from\": \"S\", \"to\": \"P\"}, \"cargoes\": [{\"id\": 0, \"route\": {\"source\": \"S\", \"destination\": \"B\"}}]}");
        JSONObject expectedReport2 = new JSONObject("{\"event\": \"transport.departed\", \"time\": 0, \"transport\": {\"id\": 2, \"type\": \"truck\"}, \"movement\": {\"from\": \"S\", \"to\": \"A\"}, \"cargoes\": [{\"id\": 1, \"route\": {\"source\": \"S\", \"destination\": \"A\"}}]}");
        Assertions.assertTrue(expectedReport1.similar(reports.get(1)));
        Assertions.assertTrue(expectedReport2.similar(reports.get(0)));
    }

    @Test
    void test4() {
        test3();
        List<JSONObject> reports = timeCalculator.timeAdvance();
        JSONObject expectedReport1 = new JSONObject("{\"event\": \"transport.arrived\", \"time\": 1, \"transport\": {\"id\": 1, \"type\": \"truck\"}, \"location\": \"P\", \"cargoes\": [{\"id\": 0, \"route\": {\"source\": \"S\", \"destination\": \"B\"}}]}");
        JSONObject expectedReport2 = new JSONObject("{\"event\": \"transport.departed\", \"time\": 1, \"transport\": {\"id\": 1, \"type\": \"truck\"}, \"movement\": {\"from\": \"P\", \"to\": \"S\"}, \"cargoes\": []}");
        JSONObject expectedReport3 = new JSONObject("{\"event\": \"transport.departed\", \"time\": 1, \"transport\": {\"id\": 0, \"type\": \"ship\"}, \"movement\": {\"from\": \"P\", \"to\": \"B\"}, \"cargoes\": [{\"id\": 0, \"route\": {\"source\": \"S\", \"destination\": \"B\"}}]}");
        Assertions.assertTrue(expectedReport1.similar(reports.get(0)));
        Assertions.assertTrue(expectedReport2.similar(reports.get(2)));
        Assertions.assertTrue(expectedReport3.similar(reports.get(1)));
    }

    @Test
    void test5() {
        test4();
        List<JSONObject> reports = timeCalculator.timeAdvance();
        JSONObject expectedReport1 = new JSONObject("{\"event\": \"transport.arrived\", \"time\": 2, \"transport\": {\"id\": 1, \"type\": \"truck\"}, \"location\": \"S\", \"cargoes\": []}");
        Assertions.assertTrue(expectedReport1.similar(reports.get(0)));
    }

    @Test
    void test6() {
        test5(); 
        List<JSONObject> reports = timeCalculator.timeAdvance();
        JSONObject expectedReport1 = new JSONObject("{\"event\": \"transport.arrived\", \"time\": 5, \"transport\": {\"id\": 2, \"type\": \"truck\"}, \"location\": \"A\", \"cargoes\": [{\"id\": 1, \"route\": {\"source\": \"S\", \"destination\": \"A\"}}]}");
        JSONObject expectedReport2 = new JSONObject("{\"event\": \"cargo.delivered\", \"time\": 5, \"cargo\": {\"id\": 1, \"route\": {\"source\": \"S\", \"destination\": \"A\"}}}");
        JSONObject expectedReport3 = new JSONObject("{\"event\": \"transport.departed\", \"time\": 5, \"transport\": {\"id\": 2, \"type\": \"truck\"}, \"movement\": {\"from\": \"A\", \"to\": \"S\"}, \"cargoes\": []}");
        JSONObject expectedReport4 = new JSONObject("{\"event\": \"transport.arrived\", \"time\": 5, \"transport\": {\"id\": 0, \"type\": \"ship\"}, \"location\": \"B\", \"cargoes\": [{\"id\": 0, \"route\": {\"source\": \"S\", \"destination\": \"B\"}}]}");
        JSONObject expectedReport5 = new JSONObject("{\"event\": \"cargo.delivered\", \"time\": 5, \"cargo\": {\"id\": 0, \"route\": {\"source\": \"S\", \"destination\": \"B\"}}}");
        JSONObject expectedReport6 = new JSONObject("{\"event\": \"transport.departed\", \"time\": 5, \"transport\": {\"id\": 0, \"type\": \"ship\"}, \"movement\": {\"from\": \"B\", \"to\": \"P\"}, \"cargoes\": []}");
        Assertions.assertTrue(expectedReport1.similar(reports.get(2)));
        Assertions.assertTrue(expectedReport2.similar(reports.get(3)));
        Assertions.assertTrue(expectedReport3.similar(reports.get(5)));
        Assertions.assertTrue(expectedReport4.similar(reports.get(0)));
        Assertions.assertTrue(expectedReport5.similar(reports.get(1)));
        Assertions.assertTrue(expectedReport6.similar(reports.get(4)));
    }

    @Test
    void test7() {
        test6();
        List<JSONObject> reports = timeCalculator.timeAdvance();
        JSONObject expectedReport1 = new JSONObject("{\"event\": \"transport.arrived\", \"time\": 9, \"transport\": {\"id\": 0, \"type\": \"ship\"}, \"location\": \"P\", \"cargoes\": []}");
        Assertions.assertTrue(expectedReport1.similar(reports.get(0)));
    }

    @Test
    void test8() {
        test7();
        List<JSONObject> reports = timeCalculator.timeAdvance();
        JSONObject expectedReport1 = new JSONObject("{\"event\": \"transport.arrived\", \"time\": 10, \"transport\": {\"id\": 2, \"type\": \"truck\"}, \"location\": \"S\", \"cargoes\": []}");
        Assertions.assertTrue(expectedReport1.similar(reports.get(0)));
    }

    private JSONObject createRoute(String source, String destination) {
        JSONObject route = new JSONObject();
        route.accumulate("source", source);
        route.accumulate("destination", destination);
        return route;
    }

    private JSONObject createSupplyJSON(JSONObject route) {
        JSONObject json = new JSONObject();
        json.accumulate("command", "cargo.supply");
        json.accumulate("route", route);
        return json;
    }

    private final TimeCalculator timeCalculator = new TimeCalculator(new Repository());
}


