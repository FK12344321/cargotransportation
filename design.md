<h1>map_model.Cargo transportation design</h1>
<h3>UML Class diagram</h3>
<img src="http://www.plantuml.com/plantuml/png/RPBHJiCW58Rl-nHSTiw-GBCqCzk5ckZCjdXZl61mt51A2T0bo-AxUu0YJVM6dFxdyFFzbALrrBY-bGMJr5go5osig6IzfAupv5fKXFN6W7A-KvC7vL1ggQx9FRMd9weNBvrGxdRwYODDWwEs9a71KkUIvlAGopES19E0MrvZzVQEgW7TrMIBgnN8T2dcnFPQoykAccDNChy-xZRFDV7hvl09pCrmo27m7JzJnQ2SeS-7K7m_rb8QZS5zetGfeXytm8L1gm75eiTt4Myf0kFbp4autHFl76LVYu3KeAve2ruqGXtplXm2DyTanjanE2QU4yhvDhJAE34U6564mmA5u04RAmxMfNDzGudn9pD07QpIZVahoCxPU8XChiam1Xxy19M3G8PRtdTRSBrH4I9fQv3Y32Q9cDoAWuHrDWnXCxkxQCYmwTq5sde86u1y2WlJFuRq4MfHLPDdN4nUTLLrqU-VHWh_jtKpVeFng6mqx4AbYTcoyZkp1CNxLf8V" alt="uml diagram">
<h3>How it works</h3>
<p>
The system emulates behaviour of the modeled map. It contains vehicles, points, and cargoes. Points store the information 
about cargoes they contain. Vehicles store information about the time it needs to return to the initial point and 
time it needs to deliver a cargo. Every time, user calls time advance, algorithm chooses the least 
time any vehicle needs to accomplish its current task. It adds this time to global time and substrates
this time from the all time variables of all the vehicles. Then the system redirects all the vehicles that 
have completed its tasks on this step.
</p>
<p>
While the program is running, it creates all the required reports as JSONObjects. It stores these 
reports in a map_model.Repository object. It also saves the report to the array of last reports. Every time 
user calls timeAdvance, current reports is emptied, so a user can access last generated reports via this variable.  
</p>
<h3>Action diagram</h3>
<img src="https://www.plantuml.com/plantuml/png/SoWkIImgAStDuNBEoKpDAr7GjLC8hYmkISsrKWWkBIZ8gLHmJYnApySY7PAPcwhWc5AMc9USoWKH14b355efE9SM5O5q1AKMv1UL52DKW4MACqloYogjuB8EgNafG5S00000" alt="Action diagram image">
<h3>API of the system</h3>
<p>
It is recommended to interact with the system only via map_model.TimeCalculator objects. 
There are two API methods: supplyCargo(JSONObject json) and timeAdvance(). Methods descriptions can be found in the corresponding .java class.
</p>